# Nagios docker image

Based on [JasonRivers/Docker-Nagios](https://github.com/JasonRivers/Docker-Nagios.git). Uses
Ubuntu 18.04 base image, installs nagios3 from apt repository.

# Usage

Build the image:

```docker build -t nagios3 .```

Start the container non-detached:

```docker run --name nagios3 -v /opt/docker/nagios3/etc:/etc/nagios3 --rm -p 8080:80 nagios```

If the volume for ```/etc/nagios3``` does not contain a nagios.cfg, it will be
populated with the distribution configuration as installed by the apt package.
However, if you pre-populate with an existing nagios.cfg, it will use that
instead.

If you want to be able to schedule checks from the web interface, make sure to
change ```check_external_commands=0``` in the default nagios.cfg to 
```check_external_commands=1```.

